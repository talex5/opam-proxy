open Lwt

(* Set OPAMFETCH to "curl --write-out %{http_code}\n -d %{url}% http://HOST:8000 -o %{out}%" *)

let cache_dir = "./cache"

let reporter =
  let report src level ~over k msgf =
    let k _ = over (); k () in
    let src = Logs.Src.name src in
    msgf @@ fun ?header ?tags:_ fmt ->
    Fmt.kpf k Fmt.stdout ("%a %a @[" ^^ fmt ^^ "@]@.")
      Fmt.(styled `Magenta string) (Printf.sprintf "%14s" src)
      Logs_fmt.pp_header (level, header)
  in
  { Logs.report = report }

let init =
  Fmt_tty.setup_std_outputs ();
  Logs.set_level (Some Logs.Info);
  Logs.set_reporter reporter

let cache = Hashtbl.create 10

let is_safe =
  let x = Array.make 256 false in
  let mark_safe c = x.(Char.code c) <- true in
  String.iter mark_safe "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
  fun c -> x.(Char.code c)

let cache_file uri =
  let buf = Buffer.create 1000 in
  let uri = Uri.to_string uri in
  let rec aux i =
    if i = String.length uri then ()
    else (
      let c = uri.[i] in
      if is_safe c then Buffer.add_char buf c
      else (
        Buffer.add_string buf (Printf.sprintf "%%%02X" (Char.code c));
      );
      aux (i + 1)
    )
  in
  aux 0;
  Filename.concat cache_dir @@ Buffer.contents buf

let save ?len body path =
  let tmp = path ^ ".new" in
  let ch = open_out_bin tmp in
  Lwt.try_bind
    (fun () ->
       Lwt.finalize
         (fun () ->
            let stream = Cohttp_lwt.Body.to_stream body in
            let rec aux got =
              Lwt_stream.get stream >>= function
              | None ->
                Option.iter (fun expected -> assert (got = expected)) len;
                Lwt.return_unit
              | Some data ->
                output_string ch data;
                aux (got + String.length data)
            in
            aux 0
         )
         (fun () -> 
            close_out ch;
            Lwt.return_unit
         )
    )
    (fun () -> Unix.rename tmp path; Lwt.return_unit)
    (fun ex -> Unix.unlink tmp; Lwt.fail ex)

let stream path =
  let i = ref 0 in
  Lwt_stream.from_direct @@ fun () ->
  let ch = open_in_bin path in
  let buf = Bytes.create (1024 * 1024) in
  Fun.protect
    ~finally:(fun () -> close_in ch)
    (fun () ->
       seek_in ch !i;
       match input ch buf 0 (Bytes.length buf) with
       | 0 -> None
       | got ->
         i := !i + got;
         Some (Bytes.sub_string buf 0 got)
    )

let rec fetch ?(ttl=8) ~path uri =
  Cohttp_lwt_unix.Client.get uri >>= fun (resp, body) ->
  let headers = Cohttp.Response.headers resp in
  match Cohttp.Response.status resp with
  | #Cohttp.Code.redirection_status ->
    if ttl < 1 then Fmt.failwith "Too many redirects (%a)" Uri.pp uri
    else (
      match Cohttp.Header.get headers "location" with
      | Some target ->
        Logs.info (fun f -> f "%a redirected to %S" Uri.pp uri target);
        fetch ~ttl:(ttl - 1) ~path (Uri.of_string target)
      | None -> Fmt.failwith "No Location header in redirect (%a)!" Uri.pp uri
    )
  | `OK ->
    begin 
      let len = Cohttp.Header.get headers "content-length" |> Option.map int_of_string in
      save body ?len path >>= fun () ->
      Lwt.return_unit
    end
  | code ->
    let msg = Fmt.strf "Failed to get %a: %s" Uri.pp uri (Cohttp.Code.string_of_status code) in
    Logs.err (fun f -> f "%s" msg);
    Lwt.fail (Failure msg)

let is_failed x =
  match Lwt.state x with
  | Lwt.Fail _ -> true
  | Lwt.Sleep | Lwt.Return _ -> false

let handle_get body =
  body |> Cohttp_lwt.Body.to_string >>= fun body ->
  match Uri.of_string body with
  | exception ex ->
    let body = Fmt.strf "Body was not a valid URI: %a" Fmt.exn ex in
    Cohttp_lwt_unix.Server.respond_error ~status:`Bad_request ~body ()
  | uri ->
    let path =
      match Hashtbl.find_opt cache uri with
      | Some data when not (is_failed data) -> data
      | _ ->
        let path = cache_file uri in
        let data =
          if Sys.file_exists path then Lwt.return path
          else (
            Logs.info (fun f -> f "Cache miss: fetching %a" Uri.pp uri);
            fetch ~path uri >|= fun () -> path
          )
        in
        Hashtbl.add cache uri data;
        data
    in
    path >>= fun path ->
    Logs.info (fun f -> f "Responding for %a" Uri.pp uri);
    let body = Cohttp_lwt.Body.of_stream (stream path) in
    Cohttp_lwt_unix.Server.respond ~status:`OK ~body ()

let server =
  let callback _conn req body =
    match Cohttp.Request.meth req with
    | `POST -> handle_get body
    | _ -> Cohttp_lwt_unix.Server.respond_error ~status:`Bad_request ~body:"Bad method" ()
  in
  Cohttp_lwt_unix.Server.create ~mode:(`TCP (`Port 8000))
    (Cohttp_lwt_unix.Server.make ~callback ())

let () =
  Lwt_main.run server
